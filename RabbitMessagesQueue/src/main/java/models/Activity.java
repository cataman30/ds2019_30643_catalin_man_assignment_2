package models;

import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Activity {

	private long patient_id;
	private String activity;
	private Date start;
	private Date end;

	public Activity(long patient_id, String activity, Date start, Date end) {
		super();
		this.patient_id = patient_id;
		this.activity = activity;
		this.start = start;
		this.end = end;
	}

	public Activity() {
		super();
	}

	public long getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(int patient_id) {
		this.patient_id = patient_id;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date date) {
		this.start = (Date) date;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date date) {
		this.end = date;
	}

	public String toJsonPrettyPrint() {
		
		String jsonInString2 = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsonInString2 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return jsonInString2;
	}

	public String toJson() {
		String jsonString = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			jsonString = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return jsonString;
	}
}

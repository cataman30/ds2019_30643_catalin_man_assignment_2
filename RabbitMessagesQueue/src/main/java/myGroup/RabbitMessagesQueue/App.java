package myGroup.RabbitMessagesQueue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import business.Business;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import models.Activity;

public class App {

	private final static String QUEUE_NAME = "messageBroker";

	public static void main(String[] args) throws Exception {
		List<Activity> lActivities = Business.getActivities();

		ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            for (Activity activity : lActivities) {
            	channel.basicPublish("", QUEUE_NAME, null, activity.toJson().getBytes("UTF-8"));
            	TimeUnit.SECONDS.sleep(1);
    		}
            //System.out.println(" [x] Sent '" + message + "'");
        }
	}
}

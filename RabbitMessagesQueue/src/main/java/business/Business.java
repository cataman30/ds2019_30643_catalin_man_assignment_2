package business;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.Activity;

public class Business {
	public static List<String> getActivitiesFromFile(String path) {
		BufferedReader reader;
		List<String> activities = new ArrayList<String>();
		try {
			reader = new BufferedReader(new FileReader(path));
			String line = reader.readLine();
			while (line != null) {
				line = reader.readLine();
				if (line != null)
					activities.add(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return activities;
	}

	public static List<Activity> getActivities() {
		List<Activity> activities = new ArrayList<Activity>();
		List<String> fileActivities = getActivitiesFromFile(
				"C:\\Users\\Administrator\\eclipse-workspace\\RabbitMessagesQueue\\activity\\activity.txt");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = null;

		for (String s : fileActivities) {
			String[] tokens = s.split("	");
			Activity activity = new Activity();

			for (int i = 0; i < 5; i++) {
				switch (i) {
				case 0:
					try {
						date = dateFormat.parse(tokens[0]);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					activity.setStart(date);
					break;
				case 2:
					try {
						date = dateFormat.parse(tokens[2]);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					activity.setEnd(date);
					break;
				case 4:
					activity.setActivity(tokens[4]);
					break;
				}
			}
			
			activity.setPatient_id(1);
			activities.add(activity);
		}

		return activities;
	}
}

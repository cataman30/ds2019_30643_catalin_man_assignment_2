package com.example.springdemo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "Medicationintake")
@Table(name = "medicationintake")
public class Medicationintake {
	
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "start")
    private Date start;
	
	@Column(name = "end")
    private Date end;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPatient")
	private Patient patient;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMedication")
	private Medication medication;
	
	public Medicationintake() {
	}
	
	public Medicationintake(Integer id, Date start, Date end, Medication medication, Patient patient) {
		super();
		this.id = id;
		this.start = start;
		this.end = end;
		this.medication = medication;
		this.patient = patient;
	}
	
	public Medicationintake(Integer id, Date start, Date end) {
		super();
		this.id = id;
		this.start = start;
		this.end = end;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Medication getMedication() {
		return medication;
	}

	public void setMedication(Medication medication) {
		this.medication = medication;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Medicationintake )) return false;
        return id != null && id.equals(((Medicationintake) o).getId());
    }
    @Override
    public int hashCode() {
        return 31;
    }
}

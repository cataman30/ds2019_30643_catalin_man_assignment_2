package com.example.springdemo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "User")
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "username", length = 45, unique = true, nullable = false)
	private String username;

	@Column(name = "password", length = 45, nullable = false)
	private String password;

	@Column(name = "role", length = 45)
	private String role;

	//DOCTORS
	@JsonManagedReference
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Doctor> doctors = new ArrayList<>();

	public void addDoctor(Doctor doctor) {
		doctors.add(doctor);
	}

	public void removeDoctor(Doctor doctor) {
		doctors.remove(doctor);
	}

	//CAREGIVERS
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Caregiver> caregivers = new ArrayList<>();

	public void addCaregiver(Caregiver caregiver) {
		caregivers.add(caregiver);
	}

	public void removeCaregiver(Caregiver caregiver) {
		caregivers.remove(caregiver);
	}

	//PATIENTS
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Patient> patients = new ArrayList<>();

	public void addPatient(Patient patient) {
		patients.add(patient);
	}

	public void removePatient(Patient patient) {
		patients.remove(patient);
	}
	
	public User() {
	}
	
	public User(String username, String password, String role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public User(Integer id, String username, String password, String role) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public User(Integer id, String username, String password) {
		this.id = id;
		this.username = username;
		this.password = password;
	}
	
	

	public User(Integer id, String username, String password, String role, List<Doctor> doctors,
			List<Caregiver> caregivers, List<Patient> patients) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
		this.doctors = doctors;
		this.caregivers = caregivers;
		this.patients = patients;
	}

	
	
	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	public List<Caregiver> getCaregivers() {
		return caregivers;
	}

	public void setCaregivers(List<Caregiver> caregivers) {
		this.caregivers = caregivers;
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}

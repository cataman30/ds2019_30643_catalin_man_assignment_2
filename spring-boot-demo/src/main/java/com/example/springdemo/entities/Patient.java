package com.example.springdemo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "Patient")
@Table(name = "patient")
public class Patient {
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "name", length = 45)
    private String name;

	@Column(name = "gender", length = 45)
    private String gender;
	
	@Column(name = "adress", length = 45)
    private String adress;
	
	@Column(name = "birthDate")
	private Date birthDate;
	
	@Column(name = "medicalRecord", length = 100)
	private String medicalRecord;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCaregiver")
	private Caregiver caregiver;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idUser")
    private User user;
    
  //MedicationIntakes
	
  	@OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, orphanRemoval = true)
  	private List<Medicationintake> medicationintakes = new ArrayList<>();

  	public void addMedicationIntake(Medicationintake medicationIntake) {
  		medicationintakes.add(medicationIntake);
  	}

  	public void removeMedicationIntake(Medicationintake medicationIntake) {
  		medicationintakes.remove(medicationIntake);
  	}

	public Patient(Integer id, String name, String gender, String adress, Date birthDate, String medicalRecord,Caregiver caregiver,
			User user, List<Medicationintake> medicationIntakes) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.adress = adress;
		this.birthDate = birthDate;
		this.medicalRecord = medicalRecord;
		this.caregiver = caregiver;
		this.user = user;
		this.medicationintakes = medicationIntakes;
	}
	
	public Patient(Integer id, String name, String gender, String adress, Date birthDate, String medicalRecord, User user) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.adress = adress;
		this.birthDate = birthDate;
		this.medicalRecord = medicalRecord;
		this.user = user;
	}
	
	public Patient(Integer id, String name, String gender, String adress, Date birthDate, String medicalRecord, User user, Caregiver caregiver) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.adress = adress;
		this.birthDate = birthDate;
		this.medicalRecord = medicalRecord;
		this.user = user;
		this.caregiver = caregiver;
	}
	
	public Patient() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public List<Medicationintake> getMedicationintakes() {
		return medicationintakes;
	}

	public void setMedicationintakes(List<Medicationintake> medicationintakes) {
		this.medicationintakes = medicationintakes;
	}

	public String getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	public Caregiver getCaregiver() {
		return caregiver;
	}

	public void setCaregiver(Caregiver caregiver) {
		this.caregiver = caregiver;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Medicationintake> getMedicationIntakes() {
		return medicationintakes;
	}

	public void setMedicationIntakes(List<Medicationintake> medicationIntakes) {
		this.medicationintakes = medicationIntakes;
	}
  	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient )) return false;
        return id != null && id.equals(((Patient) o).getId());
    }
    @Override
    public int hashCode() {
        return 31;
    }
}

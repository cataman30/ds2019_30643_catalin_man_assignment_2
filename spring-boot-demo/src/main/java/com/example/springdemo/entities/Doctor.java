package com.example.springdemo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity(name = "Doctor")
@Table(name = "doctor")
public class Doctor {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "name", length = 45)
	private String name;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUser")
	private User user;

	// CAREGIVERS

	@OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Caregiver> caregivers = new ArrayList<>();

	public void addCaregiver(Caregiver caregiver) {
		caregivers.add(caregiver);
	}

	public void removeCaregiver(Caregiver caregiver) {
		caregivers.remove(caregiver);
	}
	
	public Doctor(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Doctor(Integer id, String name, User user) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
	}

	public Doctor(Integer id, String name, User user, List<Caregiver> caregivers) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
		this.caregivers = caregivers;
	}
	
	public Doctor() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Caregiver> getCaregivers() {
		return caregivers;
	}

	public void setCaregivers(List<Caregiver> caregivers) {
		this.caregivers = caregivers;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor )) return false;
        return id != null && id.equals(((Doctor) o).getId());
    }
    @Override
    public int hashCode() {
        return 31;
    }
}

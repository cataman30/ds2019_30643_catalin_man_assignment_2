package com.example.springdemo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Activity")
@Table(name = "activity")
public class Activity {

	@Id
    @GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "patient_id")
	private Integer patient_id;
	
	@Column(name = "activity", length = 45)
	private String activity;
	
	@Column(name = "start")
	private Date start;
	
	@Column(name = "end")
	private Date end;

	public Activity(Integer patient_id, String activity, Date start, Date end, Integer id) {
		super();
		this.patient_id = patient_id;
		this.activity = activity;
		this.start = start;
		this.end = end;
		this.id = id;
	}
	
	public Activity(Integer patient_id, String activity, Date start, Date end) {
		super();
		this.patient_id = patient_id;
		this.activity = activity;
		this.start = start;
		this.end = end;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Activity() {
		super();
	}

	public long getPatient_id() {
		return patient_id;
	}

	public void setPatient_id(int patient_id) {
		this.patient_id = patient_id;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date date) {
		this.start = (Date) date;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date date) {
		this.end = date;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Activity )) return false;
        return id != null && id.equals(((Activity) o).getId());
    }
    @Override
    public int hashCode() {
        return 31;
    }
}
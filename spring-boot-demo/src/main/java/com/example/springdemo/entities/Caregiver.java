package com.example.springdemo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "Caregiver")
@Table(name = "caregiver")
public class Caregiver {
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "name", length = 45)
    private String name;

	@Column(name = "gender", length = 45)
    private String gender;
	
	@Column(name = "adress", length = 45)
    private String adress;
	
	@Column(name = "birthDate")
	private Date birthDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUser")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idDoctor")
	private Doctor doctor;
	
	//PATIENTS
	
		@OneToMany(mappedBy = "caregiver", cascade = CascadeType.ALL, orphanRemoval = true)
		private List<Patient> patients = new ArrayList<>();

		public void addPatient(Patient patient) {
			patients.add(patient);
		}

		public void removePatient(Patient patient) {
			patients.remove(patient);
		}

		public Caregiver(Integer id, String name, String gender, String adress, Date birthDate, User user,
				Doctor doctor, List<Patient> patients) {
			super();
			this.id = id;
			this.name = name;
			this.gender = gender;
			this.adress = adress;
			this.birthDate = birthDate;
			this.user = user;
			this.doctor = doctor;
			this.patients = patients;
		}
	
		public Caregiver(Integer id, String name, String gender, String adress, Date birthDate, User user) {
			super();
			this.id = id;
			this.name = name;
			this.gender = gender;
			this.adress = adress;
			this.birthDate = birthDate;
			this.user = user;
		}

		public Caregiver(){
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getAdress() {
			return adress;
		}

		public void setAdress(String adress) {
			this.adress = adress;
		}

		public Date getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(Date birthDate) {
			this.birthDate = birthDate;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public Doctor getDoctor() {
			return doctor;
		}

		public void setDoctor(Doctor doctor) {
			this.doctor = doctor;
		}

		public List<Patient> getPatients() {
			return patients;
		}

		public void setPatients(List<Patient> patients) {
			this.patients = patients;
		}
		
		@Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (!(o instanceof Caregiver )) return false;
	        return id != null && id.equals(((Caregiver) o).getId());
	    }
	    @Override
	    public int hashCode() {
	        return 31;
	    }
}

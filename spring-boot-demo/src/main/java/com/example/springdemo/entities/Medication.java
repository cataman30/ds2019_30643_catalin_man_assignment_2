package com.example.springdemo.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "Medication")
@Table(name = "medication")
public class Medication {
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "name", length = 45)
	private String name;

	@Column(name = "sideEffects", length = 200)
	private String sideEffects;

	@Column(name = "dosage", length = 45)
	private String dosage;

	@OneToMany(mappedBy = "medication", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Medicationintake> medicationintakes = new ArrayList<>();

	public void addMedicationintake(Medicationintake medicationintake) {
		medicationintakes.add(medicationintake);
	}

	public void removeMedicationintake(Medicationintake medicationintake) {
		medicationintakes.remove(medicationintake);
	}

	public Medication(Integer id, String name, String sideEffects, String dosage, List<Medicationintake> medicationintakes) {
		super();
		this.id = id;
		this.name = name;
		this.sideEffects = sideEffects;
		this.dosage = dosage;
		this.medicationintakes = medicationintakes;
	}
	
	public Medication(Integer id, String name, String sideEffects, String dosage) {
		super();
		this.id = id;
		this.name = name;
		this.sideEffects = sideEffects;
		this.dosage = dosage;
	}

	public Medication() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSideEffects() {
		return sideEffects;
	}

	public void setSideEffects(String sideEffects) {
		this.sideEffects = sideEffects;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public List<Medicationintake> getMedicationintakes() {
		return medicationintakes;
	}

	public void setMedicationintakes(List<Medicationintake> medicationintakes) {
		this.medicationintakes = medicationintakes;
	}
}

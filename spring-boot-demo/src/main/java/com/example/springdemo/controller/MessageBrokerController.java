package com.example.springdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.springdemo.services.MessageBrokerService;

@RestController
@CrossOrigin
@RequestMapping(value = "/messageBroker")
public class MessageBrokerController {

	private final MessageBrokerService mbs;
	
	@Autowired
	private MessageBrokerController(MessageBrokerService messageBrokerService) {
		mbs = messageBrokerService;
		this.listenForMessages();
	}
	
	@GetMapping()
	public boolean listenForMessages() {
		try {
			mbs.listenForMessages();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}

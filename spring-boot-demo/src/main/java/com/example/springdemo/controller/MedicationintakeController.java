package com.example.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springdemo.dto.MedicationintakeDTO;
import com.example.springdemo.services.MedicationintakeService;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationintake")
public class MedicationintakeController {
	 private final MedicationintakeService medicationintakeService;

	    @Autowired
	    public MedicationintakeController(MedicationintakeService medicationintakeService) {
	        this.medicationintakeService = medicationintakeService;
	    }

	    @GetMapping(value = "/{id}")
	    public MedicationintakeDTO findById(@PathVariable("id") Integer id){
	        return medicationintakeService.findMedicationintakeById(id);
	    }

	    @GetMapping(value="/findAll")
	    public List<MedicationintakeDTO> findAll(){
	        return medicationintakeService.findAll();
	    }

	    @PostMapping(value = "/insertMedicationintake")
	    public Integer insertMedicationintakeDTO(@RequestBody MedicationintakeDTO medicationintakeDTO){
	        return medicationintakeService.insert(medicationintakeDTO);
	    }

	    @PutMapping()
	    public Integer updateMedicationintake(@RequestBody MedicationintakeDTO medicationintakeDTO) {
	        return medicationintakeService.update(medicationintakeDTO);
	    }

	    @DeleteMapping()
	    public void delete(@RequestBody MedicationintakeDTO medicationintakeDTO){
	        medicationintakeService.delete(medicationintakeDTO);
	    }
}

package com.example.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.CaregiverService;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

	 private final CaregiverService caregiverService;

	    @Autowired
	    public CaregiverController(CaregiverService caregiverService) {
	        this.caregiverService = caregiverService;
	    }

	    @GetMapping(value = "/{id}")
	    public CaregiverDTO findById(@PathVariable("id") Integer id){
	        return caregiverService.findCaregiverById(id);
	    }

	    @GetMapping(value = "/findAllPatients/{id}")
	    public List<PatientDTO> findAllPatients(@PathVariable("id") Integer id){
	        return caregiverService.findAllPatientsForCaregiver(id);
	    }
	    
	    @GetMapping()
	    public List<CaregiverDTO> findAll(){
	        return caregiverService.findAll();
	    }

	    @PostMapping()
	    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
	        return caregiverService.insert(caregiverDTO);
	    }

	    @PutMapping()
	    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
	        return caregiverService.update(caregiverDTO);
	    }

	    @DeleteMapping()
	    public boolean delete(@RequestBody CaregiverDTO caregiverDTO){
	    	try {
	        caregiverService.delete(caregiverDTO);
	        }catch (Exception e) 
	    	{
	        	return false;
			}
	        return true;
	    }
	
}

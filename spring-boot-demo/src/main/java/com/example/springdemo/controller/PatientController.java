package com.example.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.PatientService;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {
	 private final PatientService patientService;

	    @Autowired
	    public PatientController(PatientService patientService) {
	        this.patientService = patientService;
	    }

	    @GetMapping(value = "/{id}")
	    public PatientDTO findById(@PathVariable("id") Integer id){
	        return patientService.findPatientById(id);
	    }
	    
	    @GetMapping(value = "/findPatientByUserId/{id}")
	    public List<PatientDTO> findAllPatients(@PathVariable("id") Integer id){
	        return patientService.findPacientByUserId(id);
	    }

	    @GetMapping()
	    public List<PatientDTO> findAll(){
	        return patientService.findAll();
	    }

	    @PostMapping()
	    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
	        return patientService.insert(patientDTO);
	    }

	    @PutMapping()
	    public Integer updatePatient(@RequestBody PatientDTO patientDTO) {
	        return patientService.update(patientDTO);
	    }

	    @DeleteMapping()
	    public boolean delete(@RequestBody PatientDTO patientDTO){
	    	try {
	        patientService.delete(patientDTO);
	        }catch (Exception e) 
	    	{
	        	return false;
			}
	        return true;
	    }
}

package com.example.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.services.DoctorService;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {
	private final DoctorService doctorService;

	@Autowired
	public DoctorController(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	@GetMapping(value = "/{id}")
	public DoctorDTO findById(@PathVariable("id") Integer id) {
		return doctorService.findDoctorById(id);
	}

	@GetMapping()
	public List<DoctorDTO> findAll() {
		return doctorService.findAll();
	}

	@PostMapping()
	public Integer insertDoctorDTO(@RequestBody DoctorDTO doctorDTO) {
		return doctorService.insert(doctorDTO);
	}

	@PutMapping()
	public Integer updateDoctor(@RequestBody DoctorDTO doctorDTO) {
		return doctorService.update(doctorDTO);
	}

	@DeleteMapping()
	public boolean delete(@RequestBody DoctorDTO doctorDTO) {
		try {
			doctorService.delete(doctorDTO);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}

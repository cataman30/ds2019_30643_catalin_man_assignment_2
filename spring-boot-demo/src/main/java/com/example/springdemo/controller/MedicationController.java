package com.example.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.services.MedicationService;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {
	 private final MedicationService medicationService;

	    @Autowired
	    public MedicationController(MedicationService medicationService) {
	        this.medicationService = medicationService;
	    }

	    @GetMapping(value = "/{id}")
	    public MedicationDTO findById(@PathVariable("id") Integer id){
	        return medicationService.findMedicationById(id);
	    }

	    @GetMapping()
	    public List<MedicationDTO> findAll(){
	        return medicationService.findAll();
	    }

	    @PostMapping()
	    public Integer insertMedicationDTO(@RequestBody MedicationDTO medicationDTO){
	        return medicationService.insert(medicationDTO);
	    }

	    @PutMapping()
	    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO) {
	        return medicationService.update(medicationDTO);
	    }
	    
	    @DeleteMapping()
	    public boolean delete(@RequestBody MedicationDTO medicationDTO){
	    	try {
	        medicationService.delete(medicationDTO);
	        }catch (Exception e) 
	    	{
	        	return false;
			}
	        return true;
	    }
}

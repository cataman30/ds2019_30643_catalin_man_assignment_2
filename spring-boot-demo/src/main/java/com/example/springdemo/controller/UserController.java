package com.example.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.services.UserService;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {
	 private final UserService userService;

	    @Autowired
	    public UserController(UserService userService) {
	        this.userService = userService;
	    }

	    @GetMapping(value = "/{id}")
	    public UserDTO findById(@PathVariable("id") Integer id){
	        return userService.findUserById(id);
	    }

	    @GetMapping()
	    public List<UserDTO> findAll(){
	        return userService.findAll();
	    }

	    @PostMapping()
	    public Integer insertUserDTO(@RequestBody UserDTO userDTO){
	        return userService.insert(userDTO);
	    }

	    @PutMapping()
	    public Integer updateUser(@RequestBody UserDTO userDTO) {
	        return userService.update(userDTO);
	    }

	    @DeleteMapping()
	    public void delete(@RequestBody UserDTO userDTO){
	        userService.delete(userDTO);
	    }
}

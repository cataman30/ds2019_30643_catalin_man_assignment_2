package com.example.springdemo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.hibernate.engine.query.spi.sql.NativeSQLQueryCollectionReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.UserRepository;

@Service
public class CaregiverService {

	private final CaregiverRepository caregiverRepository;
	private final UserRepository userRepository;
	private final PatientRepository patientRepository;

	@Autowired
	public CaregiverService(CaregiverRepository caregiverRepository, UserRepository userRepository,
			PatientRepository patientRepository) {
		this.caregiverRepository = caregiverRepository;
		this.userRepository = userRepository;
		this.patientRepository = patientRepository;
	}

	public CaregiverDTO findCaregiverById(Integer id) {
		Optional<Caregiver> caregiver = caregiverRepository.findById(id);

		if (!caregiver.isPresent()) {
			throw new ResourceNotFoundException("Caregiver", "user id", id);
		}
		return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
	}

	public List<CaregiverDTO> findAll() {
		List<Caregiver> caregivers = caregiverRepository.findAllOrdered();

		return caregivers.stream().map(CaregiverBuilder::generateDTOFromEntity).collect(Collectors.toList());
	}

	public List<PatientDTO> findAllPatientsForCaregiver(Integer id) {
		List<Patient> patients = patientRepository.findAll();
		List<Caregiver> caregivers = caregiverRepository.findAll();
		Integer id2 = 9999;
		for(Caregiver cg : caregivers)
			if(cg.getUser().getId() == id)
				id2 = cg.getId();
		
		List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

		for (Patient patient : patients)
			if (patient.getCaregiver().getId() == id2)
				patientDTOs.add(PatientBuilder.generateDTOFromEntity(patient));

		return patientDTOs;

	}

	private String generateRandomChars(String candidateChars, int length) {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(candidateChars.charAt(random.nextInt(candidateChars.length())));
		}

		return sb.toString();
	}

	public Integer insert(CaregiverDTO caregiverDTO) {
		String un = generateRandomChars("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", 10);
		User u1 = userRepository.save(new User(un, "CaregiverPass", "Caregiver"));
		caregiverDTO.setUs(new User(u1.getId(), u1.getUsername(), u1.getPassword(), u1.getRole()));
		return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
	}

	public Integer update(CaregiverDTO caregiverDTO) {

		Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getId());

		if (!caregiver.isPresent()) {
			throw new ResourceNotFoundException("Caregiver", "user id", caregiverDTO.getId().toString());
		}

		return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
	}

	public void delete(CaregiverDTO caregiverDTO) {

		this.caregiverRepository.deleteById(caregiverDTO.getId());

	}
}

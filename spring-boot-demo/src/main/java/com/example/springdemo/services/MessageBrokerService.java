package com.example.springdemo.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import com.example.springdemo.entities.Activity;
import com.example.springdemo.repositories.ActivityRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

@Service
public class MessageBrokerService {

	private final static String QUEUE_NAME = "messageBroker";
	private final ActivityRepository activityRepository;
	
	
	@Autowired
	public MessageBrokerService(ActivityRepository activityRepository) {
		this.activityRepository = activityRepository;
	}

	private static long calculateTimeSpent(Date dateA, Date dateB) {

		long diffInMillies = Math.abs(dateA.getTime() - dateB.getTime());

		return diffInMillies;
	}
	
	public void listenForMessages() throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			String message = new String(delivery.getBody(), "UTF-8");
			System.out.println(" [x] Received '" + message + "'");
			ObjectMapper mapper = new ObjectMapper();
			Activity act = new Activity();
			act = mapper.readValue(message, Activity.class);
			activityRepository.save(act);
			
			if ((act.getActivity().contentEquals("Leaving") || act.getActivity().contentEquals("Sleeping"))
					&& calculateTimeSpent(act.getStart(), act.getEnd()) >= 43200000) {
				System.err.println("!!!!!!!!!!!!!! -------------- "+act.getActivity() + " took more 12 hours");
			}
			if ((act.getActivity().contentEquals("Toileting") || act.getActivity().contentEquals("Showering")
					|| act.getActivity().contentEquals("Grooming"))
					&& calculateTimeSpent(act.getStart(), act.getEnd()) >= 3600000) {
				System.err.println(
						"!!!!!!!!!!!!!! -------------- The patient spent more than one hour in the bathroom. Activity: " + act.getActivity());
			}
		};
		channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
		});
	}
}

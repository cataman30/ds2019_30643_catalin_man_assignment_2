package com.example.springdemo.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import com.example.springdemo.repositories.UserRepository;

@Service
public class PatientService {

	 private final PatientRepository patientRepository;
	 private final UserRepository userRepository;

	    @Autowired
	    public PatientService(PatientRepository patientRepository, UserRepository userRepository) {
	        this.patientRepository = patientRepository;
	        this.userRepository = userRepository;
	    }

	    public PatientDTO findPatientById(Integer id){
	        Optional<Patient> patient  = patientRepository.findById(id);

	        if (!patient.isPresent()) {
	            throw new ResourceNotFoundException("Patient", "patient id", id);
	        }
	        return PatientBuilder.generateDTOFromEntity(patient.get());
	    }

	    public List<PatientDTO> findAll(){
	        List<Patient> patients = patientRepository.findAllOrdered();

	        return patients.stream()
	                .map(PatientBuilder::generateDTOFromEntity)
	                .collect(Collectors.toList());
	    }
	    
	    private String generateRandomChars(String candidateChars, int length) {
	        StringBuilder sb = new StringBuilder();
	        Random random = new Random();
	        for (int i = 0; i < length; i++) {
	            sb.append(candidateChars.charAt(random.nextInt(candidateChars
	                    .length())));
	        }

	        return sb.toString();
	    }
	    
	    public Integer insert(PatientDTO patientDTO) {
	    	String un = generateRandomChars("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", 10);
    		User u1 = userRepository.save(new User(un, "PatientPass", "Patient"));
    		patientDTO.setUs(new User(u1.getId(), u1.getUsername(), u1.getPassword(), u1.getRole()));
	        return patientRepository
	                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
	                .getId();
	    }
	    
	    public Integer update(PatientDTO patientDTO) {

	        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());

	        if(!patient.isPresent()){
	            throw new ResourceNotFoundException("Patient", "patient id", patientDTO.getId().toString());
	        }

	        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
	    }

	    public List<PatientDTO> findPacientByUserId(Integer id) {
			List<Patient> patients = patientRepository.findAll();
			
			List<PatientDTO> patientDTOs = new ArrayList<PatientDTO>();

			for (Patient patient : patients)
				if (patient.getUser().getId() == id)
					patientDTOs.add(PatientBuilder.generateDTOFromEntity(patient));

			return patientDTOs;

		}
	    
	    public void delete(PatientDTO patientDTO){
	        this.patientRepository.deleteById(patientDTO.getId());
	    }
}

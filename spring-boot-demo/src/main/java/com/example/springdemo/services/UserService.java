package com.example.springdemo.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springdemo.dto.UserDTO;
import com.example.springdemo.dto.builders.UserBuilder;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.UserRepository;

@Service
public class UserService {

	 private final UserRepository userRepository;

	    @Autowired
	    public UserService(UserRepository userRepository) {
	        this.userRepository = userRepository;
	    }

	    public UserDTO findUserById(Integer id){
	        Optional<User> user  = userRepository.findById(id);

	        if (!user.isPresent()) {
	            throw new ResourceNotFoundException("User", "user id", id);
	        }
	        return UserBuilder.generateDTOFromEntity(user.get());
	    }

	    public List<UserDTO> findAll(){
	        List<User> users = userRepository.findAllOrdered();

	        return users.stream()
	                .map(UserBuilder::generateDTOFromEntity)
	                .collect(Collectors.toList());
	    }
	    
	    public Integer insert(UserDTO userDTO) {
	    	
	        return userRepository
	                .save(UserBuilder.generateEntityFromDTO(userDTO))
	                .getId();
	    }

	    public Integer update(UserDTO userDTO) {

	        Optional<User> user = userRepository.findById(userDTO.getId());

	        if(!user.isPresent()){
	            throw new ResourceNotFoundException("User", "user id", userDTO.getId().toString());
	        }

	        return userRepository.save(UserBuilder.generateEntityFromDTO(userDTO)).getId();
	    }

	    public void delete(UserDTO userDTO){
	        this.userRepository.deleteById(userDTO.getId());
	    }
}

package com.example.springdemo.services;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.dto.builders.DoctorBuilder;
import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.User;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.DoctorRepository;
import com.example.springdemo.repositories.UserRepository;

@Service
public class DoctorService {

	private final DoctorRepository doctorRepository;
	private final UserRepository userRepository;

	@Autowired
	public DoctorService(DoctorRepository doctorRepository, UserRepository userRepository) {
		this.doctorRepository = doctorRepository;
		this.userRepository = userRepository;
	}

	public DoctorDTO findDoctorById(Integer id) {
		Optional<Doctor> doctor = doctorRepository.findById(id);

		if (!doctor.isPresent()) {
			throw new ResourceNotFoundException("Doctor", "user id", id);
		}
		return DoctorBuilder.generateDTOFromEntity(doctor.get());
	}

	public List<DoctorDTO> findAll() {
		List<Doctor> doctors = doctorRepository.findAllOrdered();
		return doctors.stream().map(DoctorBuilder::generateDTOFromEntity).collect(Collectors.toList());
	}

	private String generateRandomChars(String candidateChars, int length) {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			sb.append(candidateChars.charAt(random.nextInt(candidateChars.length())));
		}

		return sb.toString();
	}

	public Integer insert(DoctorDTO doctorDTO) {

		String un = generateRandomChars("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", 10);
		User u1 = userRepository.save(new User(un, "DoctorPass", "Doctor"));
		doctorDTO.setUser(new User(u1.getId(), u1.getUsername(), u1.getPassword(), u1.getRole()));
		return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
	}

	public Integer update(DoctorDTO doctorDTO) {

		Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getId());

		if (!doctor.isPresent()) {
			throw new ResourceNotFoundException("Doctor", "user id", doctorDTO.getId().toString());
		}

		return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
	}

	public void delete(DoctorDTO doctorDTO) {
		System.out.println("GOT HERE!!!!");
		Integer idInteger = doctorDTO.getUser().getId();
		this.userRepository.deleteById(idInteger);
		this.doctorRepository.deleteById(doctorDTO.getId());
		System.out.println("Id-ul este:" + idInteger);

	}
}

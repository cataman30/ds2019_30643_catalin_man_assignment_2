package com.example.springdemo.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springdemo.dto.MedicationintakeDTO;
import com.example.springdemo.dto.builders.MedicationintakeBuilder;
import com.example.springdemo.entities.Medicationintake;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.MedicationintakeRepository;

@Service
public class MedicationintakeService {

	 private final MedicationintakeRepository medicationintakeRepository;

	    @Autowired
	    public MedicationintakeService(MedicationintakeRepository medicationintakeRepository) {
	        this.medicationintakeRepository = medicationintakeRepository;
	    }

	    public MedicationintakeDTO findMedicationintakeById(Integer id){
	        Optional<Medicationintake> medicationintake  = medicationintakeRepository.findById(id);

	        if (!medicationintake.isPresent()) {
	            throw new ResourceNotFoundException("Medicationintake", "user id", id);
	        }
	        return MedicationintakeBuilder.generateDTOFromEntity(medicationintake.get());
	    }

	    public List<MedicationintakeDTO> findAll(){
	        List<Medicationintake> medicationintakes = medicationintakeRepository.findAllOrdered();

	        return medicationintakes.stream()
	                .map(MedicationintakeBuilder::generateDTOFromEntity)
	                .collect(Collectors.toList());
	    }
	    
	    public Integer insert(MedicationintakeDTO medicationintakeDTO) {
	    	
	        return medicationintakeRepository
	                .save(MedicationintakeBuilder.generateEntityFromDTO(medicationintakeDTO))
	                .getId();
	    }

	    public Integer update(MedicationintakeDTO medicationintakeDTO) {

	        Optional<Medicationintake> medicationintake = medicationintakeRepository.findById(medicationintakeDTO.getId());

	        if(!medicationintake.isPresent()){
	            throw new ResourceNotFoundException("Medicationintake", "user id", medicationintakeDTO.getId().toString());
	        }

	        return medicationintakeRepository.save(MedicationintakeBuilder.generateEntityFromDTO(medicationintakeDTO)).getId();
	    }

	    public void delete(MedicationintakeDTO medicationintakeDTO){
	        this.medicationintakeRepository.deleteById(medicationintakeDTO.getId());
	    }
}

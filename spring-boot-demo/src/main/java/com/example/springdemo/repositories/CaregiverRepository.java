package com.example.springdemo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.springdemo.entities.Caregiver;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer>{

	@Query(value = "SELECT u " +
            "FROM Caregiver u " +
            "ORDER BY u.name")
    List<Caregiver> findAllOrdered();
	
	Optional<Caregiver> findById(Integer integer);
	Caregiver save(Caregiver caregiver);
	void deleteById(Integer integer);
}

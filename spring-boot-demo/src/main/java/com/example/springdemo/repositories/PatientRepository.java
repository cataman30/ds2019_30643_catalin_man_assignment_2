package com.example.springdemo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.springdemo.entities.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer>{

	@Query(value = "SELECT u " +
            "FROM Patient u " +
            "ORDER BY u.name")
    List<Patient> findAllOrdered();
	
	Optional<Patient> findById(Integer integer);
	Patient save(Patient patient);
	void deleteById(Integer integer);
}

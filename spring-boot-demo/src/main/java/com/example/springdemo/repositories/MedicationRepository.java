package com.example.springdemo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.springdemo.entities.Medication;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer>{

	@Query(value = "SELECT u " +
            "FROM Medication u " +
            "ORDER BY u.name")
    List<Medication> findAllOrdered();
	
	Optional<Medication> findById(Integer integer);
	Medication save(Medication medication);
	void deleteById(Integer integer);
}

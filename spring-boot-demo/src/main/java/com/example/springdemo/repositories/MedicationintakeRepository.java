package com.example.springdemo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.springdemo.entities.Medicationintake;

@Repository
public interface MedicationintakeRepository extends JpaRepository<Medicationintake, Integer>{

	@Query(value = "SELECT u " +
            "FROM Medicationintake u ")
    List<Medicationintake> findAllOrdered();
	
	Optional<Medicationintake> findById(Integer integer);
	Medicationintake save(Medicationintake medicationintake);
	void deleteById(Integer integer);
}

package com.example.springdemo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.springdemo.entities.Doctor;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer>{

	@Query(value = "SELECT u " +
            "FROM Doctor u " +
            "ORDER BY u.name")
    List<Doctor> findAllOrdered();
	
	Optional<Doctor> findById(Integer integer);
	Doctor save(Doctor doctor);
	void deleteById(Integer integer);
}

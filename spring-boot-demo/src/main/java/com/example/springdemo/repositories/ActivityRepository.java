package com.example.springdemo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.springdemo.entities.Activity;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer>{
	
	Optional<Activity> findById(Integer integer);
	Activity save(Activity activity);
	void deleteById(Integer integer);
}
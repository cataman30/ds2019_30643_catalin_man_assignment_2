package com.example.springdemo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.springdemo.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query(value = "SELECT u " +
            "FROM User u ")
    List<User> findAllOrdered();
	
	Optional<User> findById(Integer integer);
	User save(User user);
	void deleteById(Integer integer);
}

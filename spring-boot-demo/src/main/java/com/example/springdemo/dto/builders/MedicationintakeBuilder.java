package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationintakeDTO;
import com.example.springdemo.entities.Medicationintake;

public class MedicationintakeBuilder {

	public static MedicationintakeDTO generateDTOFromEntity(Medicationintake medicationintake){
        return new MedicationintakeDTO(
                medicationintake.getId(),
                medicationintake.getStart(),
                medicationintake.getEnd());
    }

    public static Medicationintake generateEntityFromDTO(MedicationintakeDTO medicationintakeDTO){
        return new Medicationintake(medicationintakeDTO.getId(), medicationintakeDTO.getStart(), medicationintakeDTO.getEnd());
    }
}

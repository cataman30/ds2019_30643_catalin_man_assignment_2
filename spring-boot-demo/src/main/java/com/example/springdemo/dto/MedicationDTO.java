package com.example.springdemo.dto;

import java.util.Objects;

public class MedicationDTO {

	private Integer id;
	private String name;
	private String sideEffects;
	private String dosage;
	
	public MedicationDTO(Integer id, String name, String sideEffects, String dosage) {
		super();
		this.id = id;
		this.name = name;
		this.sideEffects = sideEffects;
		this.dosage = dosage;
	}
	
	public MedicationDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSideEffects() {
		return sideEffects;
	}

	public void setSideEffects(String sideEffects) {
		this.sideEffects = sideEffects;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO medicationDTO = (MedicationDTO) o;
        return Objects.equals(id, medicationDTO.id) &&
                Objects.equals(name, medicationDTO.name) &&
                Objects.equals(sideEffects, medicationDTO.sideEffects) &&
                Objects.equals(dosage, medicationDTO.dosage);
        }
    
    @Override
    public int hashCode() {

        return Objects.hash(id, name, sideEffects, dosage);
    }
}

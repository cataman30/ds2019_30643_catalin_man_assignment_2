package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.entities.Patient;

public class PatientBuilder {

	public static PatientDTO generateDTOFromEntity(Patient patient) {
		return new PatientDTO(patient.getId(), patient.getName(), patient.getGender(), patient.getAdress(),
				patient.getBirthDate(), patient.getMedicalRecord(), patient.getUser(), patient.getCaregiver());
	}

	public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
		return new Patient(patientDTO.getId(), patientDTO.getName(), patientDTO.getGender(), patientDTO.getAdress(),
				patientDTO.getBirthDate(), patientDTO.getMedicalRecord(), patientDTO.getUs(), patientDTO.getCg());
	}
}

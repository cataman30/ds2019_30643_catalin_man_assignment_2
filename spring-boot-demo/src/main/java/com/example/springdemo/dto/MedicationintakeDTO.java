package com.example.springdemo.dto;

import java.sql.Date;
import java.util.Objects;

public class MedicationintakeDTO {

	private Integer id;
	private Date start;
	private Date end;
	
	public MedicationintakeDTO(Integer id, Date start, Date end) {
		super();
		this.id = id;
		this.start = start;
		this.end = end;
	}
	
	public MedicationintakeDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationintakeDTO medicationintakeDTO = (MedicationintakeDTO) o;
        return Objects.equals(id, medicationintakeDTO.id) &&
                Objects.equals(start, medicationintakeDTO.start) &&
                Objects.equals(end, medicationintakeDTO.end);
    }
    
    @Override
    public int hashCode() {

        return Objects.hash(id, start, end);
    }
}

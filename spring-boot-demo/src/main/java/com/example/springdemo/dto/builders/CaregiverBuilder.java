package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.entities.Caregiver;

public class CaregiverBuilder {
	
	 public CaregiverBuilder() {
	    }

	    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
	        return new CaregiverDTO(
	                caregiver.getId(),
	                caregiver.getName(),
	                caregiver.getGender(),
	                caregiver.getAdress(),
	                caregiver.getBirthDate(),
	                caregiver.getUser());
	    }

	    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
	        return new Caregiver(caregiverDTO.getId(), caregiverDTO.getName(), caregiverDTO.getGender(), caregiverDTO.getAdress(), caregiverDTO.getBirthDate(), caregiverDTO.getUs());
	    }
}

package com.example.springdemo.dto;

import java.sql.Date;
import java.util.Objects;

import com.example.springdemo.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class CaregiverDTO {

	private Integer id;
	private String name;
	private String gender;
	private String adress;
	private Date birthDate;
	@JsonIgnore
	private User us;
	
	public CaregiverDTO(Integer id, String name, String gender, String adress, Date birthDate, User us) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.adress = adress;
		this.birthDate = birthDate;
		this.us = us;
	}
	
	public CaregiverDTO() {
	}
	
	public User getUs() {
		return us;
	}

	public void setUs(User us) {
		this.us = us;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO caregiverDTO = (CaregiverDTO) o;
        return Objects.equals(id, caregiverDTO.id) &&
                Objects.equals(name, caregiverDTO.name) &&
                Objects.equals(gender, caregiverDTO.gender) &&
                Objects.equals(birthDate, caregiverDTO.birthDate) &&
                Objects.equals(adress, caregiverDTO.adress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, gender, birthDate, adress);
    }

	
}

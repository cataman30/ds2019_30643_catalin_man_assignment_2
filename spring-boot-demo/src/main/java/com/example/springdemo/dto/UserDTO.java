package com.example.springdemo.dto;

import java.util.Objects;

import com.example.springdemo.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDTO {

	private Integer id;
	private String username;
	private String password;
	private String role;
	private User us;

	public UserDTO(Integer id, String username, String password, String role) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public UserDTO(String username, String password, String role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public UserDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDTO userDTO = (UserDTO) o;
		return Objects.equals(id, userDTO.id) && Objects.equals(username, userDTO.username)
				&& Objects.equals(password, userDTO.password) && Objects.equals(role, userDTO.role);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, username, password, role);
	}
}

package com.example.springdemo.dto;

import java.util.Objects;

import com.example.springdemo.entities.User;

public class DoctorDTO {

	private Integer id;
	private String name;
	// @JsonIgnore
	private User user;

	public DoctorDTO(Integer id, String name, User user) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
	}

	@Override
	public String toString() {
		return "DoctorDTO [id=" + id + ", name=" + name + ", us=" + user + "]";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User us) {
		this.user = us;
	}

	public DoctorDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		DoctorDTO doctorDTO = (DoctorDTO) o;
		return Objects.equals(id, doctorDTO.id) && Objects.equals(name, doctorDTO.name);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, name);
	}
}

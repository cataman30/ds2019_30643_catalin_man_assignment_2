package com.example.springdemo.dto;

import java.sql.Date;
import java.util.Objects;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class PatientDTO {

	private Integer id;
	private String name;
	private String gender;
	private String adress;
	private Date birthDate;
	private String medicalRecord;
	@JsonIgnore
	private User us;
	@JsonIgnore
	private Caregiver cg;
	
	public PatientDTO(Integer id, String name, String gender, String adress, Date birthDate, String medicalRecord, User us) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.adress = adress;
		this.birthDate = birthDate;
		this.medicalRecord = medicalRecord;
		this.us = us;
	}
	
	public PatientDTO(Integer id, String name, String gender, String adress, Date birthDate, String medicalRecord, User us, Caregiver cg) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.adress = adress;
		this.birthDate = birthDate;
		this.medicalRecord = medicalRecord;
		this.us = us;
		this.cg = cg;
	}
	
	public Caregiver getCg() {
		return cg;
	}

	public void setCg(Caregiver cg) {
		this.cg = cg;
	}

	public User getUs() {
		return us;
	}

	public void setUs(User us) {
		this.us = us;
	}

	
	public PatientDTO() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

    public String getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(id, patientDTO.id) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(gender, patientDTO.gender) &&
                Objects.equals(birthDate, patientDTO.birthDate) &&
                Objects.equals(adress, patientDTO.adress);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, gender, birthDate, adress);
    }
}
